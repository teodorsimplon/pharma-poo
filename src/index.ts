import Client from './class/Client';
import Main from './class/Main';
import Medicament from './class/Medicament';
import Utils from './class/Utils';
import Vaccin from './class/Vaccin';

const maPharmacie = new Main();

maPharmacie.ajouterClient('bob', 100, {numero:25, rue: 'Chemin citron', codePostal: 31410, ville: 'Longages'});
maPharmacie.ajouterClient('jay', 200, {numero:44, rue: 'Rue de spinach', codePostal: 31370, ville: 'Carbonne'});
maPharmacie.ajouterClient('coco', 5000, {numero:66, rue: 'Rue de fun', codePostal: 31333, ville: 'Imaginaire'} );



maPharmacie.ajouterMedicament("Aspirin", 5, 2);
maPharmacie.ajouterMedicament("VERSIGUARD RABIES",2 , 1);
maPharmacie.ajouterMedicament("SalAd",6 , 100);
maPharmacie.ajouterMedicament("SinCarne",6 , 0);

maPharmacie.ajouterVaccin("Avaxim 160", 21, 10, "Hépatite A");
maPharmacie.ajouterVaccin("Boostrixtetra", 21, 10, "Diphtérie, Tétanos, Poliomyélite, Coqueluche");
maPharmacie.ajouterVaccin("Ixiaro", 21, 10, "Encéphalite japonaise");
maPharmacie.ajouterVaccin("RVaxpro", 21, 10, "Rougeole,  Oreillons,  Rubéole");

const client= maPharmacie.rechercheClient("bob");
const client2= maPharmacie.rechercheClient("jay");
const medicament= maPharmacie.rechercheMedicament("Aspirin")
const vaccin= maPharmacie.rechercheVaccin("Ixiaro")

client.acheterMedicament(medicament);
client2.acheterVaccin(vaccin);
client2.acheterVaccin(vaccin);

//remplir stock avec 10 
// maPharmacie.remplirStockMed(1)

// maPharmacie.stockMedZero().forEach(medicament=> medicament.augmenterStock(10))
Utils.consoleArray(maPharmacie.stockMedZero());
//maPharmacie.rechercheClientVaccine([])

// console.log(maPharmacie);
//  console.log(client2);

 
