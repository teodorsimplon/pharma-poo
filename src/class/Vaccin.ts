import Medicament from './Medicament';

export default class Vaccin extends Medicament {

    private type: string;

    constructor(nom: string, prix: number, stock: number, type: string) {

        super(nom, prix, stock)
        this.type = type;
    }

    getType() {
        return this.type;
    }
}
