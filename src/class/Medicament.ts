export default class Medicament {

    private nom: string;
    private prix: number;
    private stock: number;
    constructor(nom: string, prix: number, stock: number) {
        this.nom = nom;
        this.prix = prix;
        this.stock = stock;
    }
    public augmenterStock(stock: number) {
        this.stock = this.stock + stock;
    }
    public diminuerStock(stock: number) {
        this.stock -= stock;
    }
    public getStock() {
        return this.stock;
    }
    public setStock(stock: number) {
        this.stock = stock;
    }
    public getPrix() {
        return this.prix;
    }
    public getNom() {
        return this.nom
    }
    public setNom(nom: string) {
        this.nom = nom;
    }
}