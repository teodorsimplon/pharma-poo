import IAdresse from "../models/IAdresse";
import Medicament from "./Medicament";
import Vaccin from "./Vaccin";

export default class Client {
    private nom: string;
    private credit: number;

    private vaccin: string[]= [];

    private adresse: IAdresse;

    constructor(nom: string, credit: number, adrs: IAdresse) {
        this.nom = nom;
        this.credit = credit;
        this.adresse = adrs;
    }
    getVaccin() {
        return this.vaccin;
    }

    augmenterCredit(credit: number): void  // : void indique que cette methode ne renvoie rien
    {
        this.credit += credit;
    }

    diminuerCredit(credit: number): void // : void indique que cette methode ne renvoie rien
    {
        this.credit -= credit;
    }

    getCredit(): number // : number indique que cette methode renvoie un nombre
    {
        return this.credit;
    }

    getNom(): string // : string indique que cette methode renvoie une chaine de caractères
    {
        return this.nom;
    }

    getAdresse():IAdresse
    {
        return this.adresse

    }
    acheterMedicament(medicament: Medicament) {

        if (medicament.getStock() > 0 && this.getCredit() > medicament.getPrix()) {

            //reduire stock
            medicament.setStock(medicament.getStock() - 1);

            //reduir credit client
            this.diminuerCredit(medicament.getPrix());
            // console.log(medicament)
        }
        else {
            console.log("insuficient stock medicament")
        }
    }
    acheterVaccin(vaccin: Vaccin) {

        if (this.vaccin.includes(vaccin.getType())){
            console.log("deja vacciné")

            return;
        }

        if (vaccin.getStock() > 0 && this.getCredit() > vaccin.getPrix()) {

            //reduire stock
            vaccin.setStock(vaccin.getStock() - 1);

            //reduir credit client
            this.diminuerCredit(vaccin.getPrix());
            // console.log(medicament)

            this.vaccin.push(vaccin.getType())
        }
        else {
            console.log("insuficient stock vaccin")
        }
    } 

 
}