import IAdresse from "../models/IAdresse";
import Personne from "./Personne";

export default class PersonnelMedical extends Personne {
    adresse: IAdresse;
    nom: string;
    constructor(adresse: IAdresse, nom: string) {
        super()
        this.adresse = adresse;
        this.nom = nom


    }

    public getNom(): string {
        return this.nom

    }
}