import IAdresse from '../models/IAdresse';
import Client from './Client';
import Medicament from './Medicament';
import Utils from './Utils';
import Vaccin from './Vaccin';

export default class Main {
    private clients: Client[] = []; // : Client[] indique que la propriete privée clients ne peut contenir que des objets de type Client
    private medicaments: Medicament[] = []; // pareil pour les medicaments

    private vaccins: Vaccin[] = [];

    ajouterClient(nom: string, credit: number, adresse: IAdresse) {
        const nouveauClient: Client = new Client(nom, credit, adresse);

        this.clients.push(nouveauClient);

        // console.log(this.clients)
    }
    ajouterMedicament(nom: string, prix: number, stock: number) {
        const nouveauMedicament: Medicament = new Medicament(nom, prix, stock);

        this.medicaments.push(nouveauMedicament);
        Utils.consoleArray(this.medicaments)
    }

    ajouterVaccin(nom: string, prix: number, stock: number, type: string) {
        const nouveauVaccin: Vaccin = new Vaccin(nom, prix, stock, type);
        this.vaccins.push(nouveauVaccin);
        console.log(this.vaccins)
    }


    //  methode pour chercher client dans l'array par name 
    rechercheClient(nom: string) {
        return this.clients.filter(client => client.getNom() == nom)[0];

    }

    rechercheMedicament(nom: string) {
        const resultMedicament = this.medicaments.filter(medicament => medicament.getNom() == nom)[0];
        return resultMedicament
    }

    rechercheVaccin(nom: string) {
        const resultVaccin = this.vaccins.filter(vaccin => vaccin.getNom() == nom)[0];
        return resultVaccin
        // console.log(resultVaccin);

    }
    rechercheClientVaccine(vaccineAvec: string | undefined) {
        return this.clients.filter(client => client.getVaccin())

    }

    stockMedZero() {
        return this.medicaments.filter(medicament => medicament.getStock() === 0)
    }

    remplirStockMed(stock: number) {
        this.medicaments.filter(medicament => medicament.getStock() === 0).forEach(element => {
            element.augmenterStock(stock * 10);
            console.log("remplir stock des medicaments", element.getNom(), element.getStock());
        })
    }

}
