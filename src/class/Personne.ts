import IAdresse from "../models/IAdresse";

export default abstract class Personne {
    abstract nom: string;
    abstract adresse: IAdresse;

    public abstract getNom(): string
}